#!/usr/bin/env python

from common import *

# 8- and 9-digit pandigital numbers can't be primes, 
# because they are all divisible by three.
# 1+2+3+4+5+6+7+8   = 4*9 = 36
# 1+2+3+4+5+6+7+8+9 = 36+9 = 45
primes = makeMap( makePrimes(10000000) )
perms = permutations( digits(1234567) )

print max( [ makeNum(x) for x in perms if makeNum(x) in primes ] )

