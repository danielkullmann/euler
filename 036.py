#!/usr/bin/env python

"""
"""

from common import *

def binary(n):
  if n == 0: return "0"
  r = ""
  while n > 0:
    if n%2 == 0:
      r += "0"
    else:
      r += "1"
    n = n/2
  return r[::-1]

sm = 0
for a in range(1,1000000):
  s = str(a)
  if s == s[::-1]:
    b = binary(a)
    if b == b[::-1]:
      sm += a
      print a

print sm

