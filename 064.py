#!/usr/bin/env python

"""
All square roots are periodic when written as continued fractions and can be written in the form:
sqrt N = a0 + 1
  	      a1 + 1
  	  	       a2 + 1
           	  	  	a3 + ...

It can be seen that the sequence is repeating. For conciseness, we use the
notation sqrt 23 = [4;(1,3,1,8)], to indicate that the block (1,3,1,8) repeats
indefinitely.

The first ten continued fraction representations of (irrational) square roots are:

sqrt 2=[1;(2)], period=1
sqrt 3=[1;(1,2)], period=2
sqrt 5=[2;(4)], period=1
sqrt 6=[2;(2,4)], period=2
sqrt 7=[2;(1,1,1,4)], period=4
sqrt 8=[2;(1,4)], period=2
sqrt 10=[3;(6)], period=1
sqrt 11=[3;(3,6)], period=2
sqrt 12= [3;(2,6)], period=2
sqrt 13=[3;(1,1,1,1,6)], period=5

Exactly four continued fractions, for N <= 13, have an odd period.

How many continued fractions for N <= 10000 have an odd period?

"""

from math import sqrt

def irrfractions(x):
  result = []
  root = int(sqrt(x))
  maxb = root
  a, b, c = root, root, 1
  # a + ((sqrt(x) - b ) / c
  rests = []
  while (-b,c) not in rests:
    rests.append((-b,c))
    result.append(a)
    if (x-b**2) % c != 0: raise Exception("a")
    u, v = b, (x-b**2)/c
    # (sqrt(x) + u ) / v
    for b in range(root,0,-1):
      if (u+b) % v == 0:
        a = (u+b)/v
        break
    if (u+b) % v != 0:
      raise Exception("b " + str((x,u,b,v,maxb)))
    c = v
  result.append(a)

  if (-b,c) != rests[0]:
    raise Exception("c")
  return result

cnt = 0
for n in range(2,10001):
  root = int(sqrt(n))
  if root*root == n:
    # not irrational
    continue
  irr = irrfractions(n)
  print n, irr
  if len(irr) % 2 == 0:
    cnt += 1

print cnt

# Solved: 1322

