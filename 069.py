#!/usr/bin/python

"""
"""

from fractions import gcd

def phi(n):
  r = 0
  for k in range(1,n):
    if gcd(n,k) == 1: r += 1
  return r

mx = 0

# easy guess:

# This comes from the observation that numbers like 6 (1*2*3)
# and 480 (1*2*3*4*5*6*..) have very low phi numbers, so I just
# tried to create such a number
i = 2
l = 2
while l < 1000000:
  p = phi(l)
  r = float(l) / float(p)
  if r > mx:
    mx = r
    print i, l, p, r
  # Now skip any i's that share already too many
  # divisors with l
  while True: 
    i += 1
    if gcd(l,i) == 1: break
  l *= i
  print i, l


#print "slowwwwww"
#
#for l in range(2,1000001):
#  p = phi(l)
#  r = float(l) / float(p)
#  if r > mx:
#    mx = r
#    print l, p, r

# Solved: 19 9699690

