#!/usr/bin/python

"""
"""

sumv=2
primes = [2]
n=3
while n < 2000000:
  prime = True
  for p in primes:
    if (n % p) == 0:
      prime = False
      break
  if prime:
    primes.append(n)
    sumv += n
  # 2 is the only even prime
  n += 2

print sumv


