#!/usr/bin/env python

numbers = " one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen".split(' ')

tens = " ten twenty thirty forty fifty sixty seventy eighty ninety".split(' ')

factors = "one ten hundred thousand".split(' ')

def saynumber(number):
  if number < 20: return numbers[number]
  if number < 100:
    ten = number / 10
    one = number % 10
    if one == 0:
      return tens[ten]
    else:
      return tens[ten] + "-" + numbers[one]
    pass
  if number < 1000:
    hundreds = number / 100
    rest = number % 100
    if rest == 0:
      return numbers[hundreds] + " hundred"
    else:
      return numbers[hundreds] + " hundred and " + saynumber(rest)
    pass
  if number == 1000:
    return "one thousand"
  return None

l = 0
for i in range(1,1001):
  m = saynumber(i)
  n = m.replace(' ', '')
  n = n.replace('-', '')
  l += len(n)
  print i, m, len(n), l

print l

# Solved: 21124

