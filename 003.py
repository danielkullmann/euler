#!/usr/bin/python

"""
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
"""

import os,sys

number = 600851475143
origNumber = number

factor = None

#os.system("factor 600851475143")

primes = [2]
print 2, 
n=3
while n*n < origNumber:
  prime = True
  for p in primes:
    if (n % p) == 0:
      prime = False
      break
  if prime:
    primes.append(n)
    if (number % n) == 0:
      print "*", n,
      sys.stdout.flush()
      factor = n
      number = number / n
      if number == 1:
        break;
  # 2 is the only even prime
  n += 2

print
print factor

# Solved: 2 * 71 * 839 * 1471 * 6857 => 6857

