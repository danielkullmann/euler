#!/usr/bin/env python

from common import *

fh = open("keylog.txt", "r")
lines = fh.readlines()
fh.close()

log = [ [int(c) for c in l.strip()] for l in lines ]

#orders = []
#for l in log:
#  for i in range(0,len(l)-1):
#    orders.append( (l[i], l[i+1]) )
#
#for o in orders: print o
#
#seq = orders[0]
#orders = orders[1:]
#
#while len(orders) > 0:
#  nOrders = []
#  for o in orders:
#    pass
#  orders = nOrders

def containedIn(a, b, ai, bi):
  if len(b) > len(a): return containedIn(b,a,bi,ai)
  if len(a) == len(b): return None
  assert len(a) > len(b)
  for i in range(0,len(a)-len(b)+1):
    if b == a[i:i+len(b)]:
      return bi
  return None

def find(lst, offset, val):
  for i,v in enumerate(lst[offset:]):
    if v == val: 
      return offset+i
  return None

def sameSeq( master, client ):
  if len(client) >= len(master):
    return False
  im = -1 
  # Are all vaules of clint also in master, in the same order?
  for ic in range(0,len(client)):
    im = find(master, im+1, client[ic])
    if im == None: return False
  return True

while True:
  deleteLog = []
  newLog = []
  for i in range(0,len(log)):
    for j in range(0,len(log)):
      if i == j: continue

      li = len(log[i])
      lj = len(log[j])

      if sameSeq(log[i], log[j]):
        deleteLog.append(j)
        continue

      if sameSeq(log[j], log[i]):
        deleteLog.append(i)
        continue

      for k in range(li-1,-1,-1):
        if log[i][k:] == log[j][0:li-k]:
          newLog.append( log[i] + log[j][li-k:] )
          deleteLog.append(i)
          deleteLog.append(j)

      for k in range(lj-1,-1,-1):
        if log[j][k:] == log[i][0:lj-k]:
          newLog.append( log[j] + log[i][lj-k:] )
          deleteLog.append(i)
          deleteLog.append(j)

      k = containedIn( log[i], log[j], i, j )
      if k != None:
        deleteLog.append(k)

  if len(deleteLog) > 0:
    k = []
    for i,l in enumerate(log):
      if not i in deleteLog:
        k.append(l)
    for l in newLog:
      if l not in k:
        k.append( l )
    log = k
    #print log
  else:
    break

for l in log:
  print ''.join( [ str(i) for i in l] )

# Solved: 73162890

