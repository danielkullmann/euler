#!/usr/bin/env python

import itertools
p = itertools.permutations( range(0,10) )
for i in range(0,1000000-1): p.next()
print ''.join( [str(c) for c in p.next()] )


