#!/usr/bin/env python

sum = 0
sumOfSquares = 0
for i in range(1,101):
  sum += i
  sumOfSquares += i*i

print sum*sum-sumOfSquares

# Solved: 25164150

