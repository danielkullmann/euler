#!/usr/bin/env python

def chain(n):
  l = 1
  while n != 1:
    if n % 2 == 0:
      n = n / 2
    else:
      n = 3*n + 1
    l += 1
  return l

maxv = 0
for i in range(1,1000000):
  l = chain(i)
  if l > maxv:
    print i, l
    maxv = l

# Solved: 837799 525

