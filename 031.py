#!/usr/bin/env python

from common import *

"""
"""

denominations = [200, 100, 50, 20, 10, 5, 2, 1]

def combine( value, idx=0 ):
  if denominations[idx] == 1: return 1
  sum = 0
  while True:
    sum += combine(value, idx+1)
    value -= denominations[idx]
    if (value < 0): break
  return sum


print combine( 200 )

