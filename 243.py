#!/usr/bin/env python

"""
A positive fraction whose numerator is less than its denominator is called a proper fraction.
For any denominator, d, there will be d-1 proper fractions; for example, with d = 12:
1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12.

We shall call a fraction that cannot be cancelled down a resilient fraction.
Furthermore we shall define the resilience of a denominator, R(d), to be the
ratio of its proper fractions that are resilient; for example, R(12) = 4/11.
In fact, d = 12 is the smallest denominator having a resilience R(d) < 4/10.

Find the smallest denominator d, having a resilience R(d) < 15499/94744.
"""

from fractions import Fraction as F, gcd
from common import makePrimes

target = F(15499,94744)

def resilience(n):
  cnt = 0
  for i in range(1,n):
    if gcd(i,n) == 1: cnt+=1
  return F(cnt,n-1)

d = 1
p = 2
while True:
  d2 = d * p / gcd(d,p)
  if d2 != d:
    d = d2
    print d
    #res = resilience(d)
    #print d, res
    #if res < target:
    #  break
    if p > 40: break
  p+=1

