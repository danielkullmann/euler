#!/usr/bin/env python

from common import *


pr = makePrimes(1000000)

x = [p for p in pr if p > 2 and p < 1000]
bs = []
for e in x: 
  bs.append(e)
  bs.append(-e)

mx = 0
for a in range(-998,998,2):
  b = 2
  n = 0
  while True:
    k = n*n + a*n + b
    if k > 2 and k%2 == 0:
      break
    if k not in pr:
      break
    n += 1
  n -= 1
  if n > mx:
    mx = n
    print "==>", a, b, n

def f(a):
  global mx
  for b in bs:
    n = 0
    while True:
      k = n*n + a*n + b
      if k > 2 and k%2 == 0:
        break
      if k not in pr:
        break
      n += 1
    n -= 1
    if n > mx:
      mx = n
      print "==>", a, b, n

for a in range(1,999,2):
  print a
  f(a)

for a in range(-1,-999,-2):
  print a
  f(a)

#import multiprocessing as mp
#pool = mp.Pool(4)
#result = pool.map( f, range(-999,999,2) )

