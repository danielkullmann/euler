#!/usr/bin/env python

""" 
If a box contains twenty-one coloured discs, composed of fifteen blue discs
and six red discs, and two discs were taken at random, it can be seen that the
probability of taking two blue discs, P(BB) = (15/21)*(14/20) = 1/2.

The next such arrangement, for which there is exactly 50% chance of taking two
blue discs at random, is a box containing eighty-five blue discs and
thirty-five red discs.

By finding the first arrangement to contain over 10^12 = 1,000,000,000,000 discs
in total, determine the number of blue discs that the box would contain.  
"""

# n = number of blue discs
# m = number of red discs
# p proability that one takes two blue discs out
# p = 0.5 
# 0.5 = n/(n+m) * (n-1)/(n+m-1)
#     = n*(n-1) / ((n+m)*(n+m-1))
# 0.5 * ((n+m)*(n+m-1)) = n*(n-1)
# 0.5 * (n^2 + nm - n + nm + m^2 -m ) = n^2 - n
# 0.5 * (n^2 + m^2 + 2nm - n - m ) - n^2 + n = 0
# -0.5 n^2 + 0.5 m^2 + nm + 0.5 n - 0.5 m = 0
# -n^2 + m^2 + 2nm + n -m = 0 
## Treat m as a constant, and n as variable to solve for:
## -n^2 + (2m + 1) n + m^2 - m = 0

# a = number of blue discs
# n = number of all discs
# a/n * (a-1)/(n-1) = 1/2
# (a^2 - a) / (n*(n-1)) = 1/2
# (a^2 - a) / (n^2 - n) = 1/2
# 2*a^2 - 2*a = n^2 - n

