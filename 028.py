#!/usr/bin/env python

from common import *

"""
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
"""

# The one in the middle
sum = 1

topRight = 1
size = 1

while size < 1001:
  size += 2
  # 
  bottomRight = topRight + 1 + size-2
  bottomLeft = bottomRight + size-1
  topLeft = bottomLeft + size-1
  topRight = topLeft + size-1
  #print bottomRight , bottomLeft , topLeft, topRight
  sum += bottomRight + bottomLeft + topLeft + topRight
  print size, sum


