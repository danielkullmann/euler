#!/usr/bin/env python

import fractions
g = 1
for i in range(1,21): g = g*i/fractions.gcd(g,i)
print g

# Solved: 232792560

