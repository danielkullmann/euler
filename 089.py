#!/usr/bin/env python

from common import *

fh = open("roman.txt", "r")
lines = fh.readlines()
fh.close()

numerals = [
  ('M', 1000),
  ('CM', 900),
  ('D',  500),
  ('CD', 400),
  ('C',  100),
  ('XC',  90),
  ('L',   50),
  ('XL',  40),
  ('X',   10),
  ('IX',   9),
  ('V',    5),
  ('IV',   4),
  ('I',    1)
]

def value(c):
  for n in numerals:
    if c == n[0]:
      return n[1]
  raise Exception( c + " not recognized")

def readRomanNumber(s):
  val = 0
  i = 0
  while i < len(s):
    cval = value(s[i])
    if i<len(s)-1 and value(s[i+1]) > cval:
      val += value(s[i+1]) - cval
      i += 1
    else:
      val += cval
    i += 1
  return val

def makeRomanNumber(n):
  s = ""
  while n > 0:
    for item in numerals:
      if n >= item[1]:
        s += item[0]
        n = n - item[1]
        break
  return s

saved = 0
for l in lines:
  l = l.strip()
  num = readRomanNumber(l)
  min = makeRomanNumber(num)
  print l, num, min
  saved += len(l) - len(min)

print saved

# Solved: 743

