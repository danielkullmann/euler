#!/usr/bin/env python

"""
Each character on a computer is assigned a unique code and the preferred
standard is ASCII (American Standard Code for Information Interchange). For
example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII,
then XOR each byte with a given value, taken from a secret key. The advantage
with the XOR function is that using the same encryption key on the cipher text,
restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text
message, and the key is made up of random bytes. The user would keep the
encrypted message and the encryption key in different locations, and without
both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified
method is to use a password as a key. If the password is shorter than the
message, which is likely, the key is repeated cyclically throughout the
message. The balance for this method is using a sufficiently long password key
for security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower
case characters. Using cipher1.txt (right click and 'Save Link/Target As...'),
a file containing the encrypted ASCII codes, and the knowledge that the plain
text must contain common English words, decrypt the message and find the sum of
the ASCII values in the original text.
"""

import sys
from common import *

def keys():
  for a in range(ord('a'),ord('z')+1):
    for b in range(ord('a'),ord('z')+1):
      for c in range(ord('a'),ord('z')+1):
        yield (a, b, c)


fh = open( "cipher1.txt", "r" )
lines = fh.readlines()
fh.close()

ASCII = " ,.*+0123456789"
for a in range(ord('a'),ord('z')+1):
  ASCII += chr(a)
for a in range(ord('A'),ord('Z')+1):
  ASCII += chr(a)

chars = [ int(i) for i in "".join(lines).split(",") ]

mxAscii = 0
text = ""

for key in keys():
  #print key
  decrypted = ""
  kidx = 0
  for c in chars:
    decrypted += chr( c ^ key[kidx%3] )
    kidx += 1
    
  numAscii = len( [d for d in decrypted if d in ASCII] )
  if numAscii > mxAscii:
    mxAscii = numAscii
    text = decrypted
    print key, numAscii, decrypted
  #elif numAscii > 9 * len(chars) / 10:
  #  print key, numAscii, decrypted
    

print sum( [ ord(c) for c in text ] )

# Solved: 107359 

