#!/usr/bin/env python

import sys
from common import *

nt = 1
np = 1
nh = 1

def nextTri():
  global nt
  v = nt*(nt+1)/2
  nt += 1
  return v

def nextPent():
  global np
  v = np*(3*np-1)/2
  np += 1
  return v

def nextHex():
  global nh
  v = nh*(2*nh-1)
  nh += 1
  return v

t = nextTri()
p = nextPent()
h = nextHex()

cnt = 0
while True:
  if t == p and p == h:
    print "==", t, nt, np, nh
    if t > 40755:
      break

  m = min([t,p,h])

  #print m,t,p,h,nt,np,nh

  if t == m: t = nextTri()
  if p == m: p = nextPent()
  if h == m: h = nextHex()

  cnt += 1
  #if cnt % 1000000 == 0:
  #  print cnt/1000000, t, p, h


