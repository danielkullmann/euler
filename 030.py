#!/usr/bin/env python

def digits(n): return [int(c) for c in str(n)]
def a(n): return sum( [b**5 for b in digits(n)] )

i=2
s=0
while True:
  if i == a(i):
    s += i
    print s, i, [b**5 for b in digits(i)]
  i += 1
  # I know the largest number is 194979
  # (because I ran the script without the following lines)
  if i > 200000:
    break

