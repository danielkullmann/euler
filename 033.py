#!/usr/bin/env python

"""
"""

from common import *

r = []

def f(a1,b1,a2,b2):
  g1 = gcd(a1,b1)
  g2 = gcd(a2,b2)
  if a1/g1 == a2/g2 and b1/g1 == b2/g2:
    print a1, b1, a2, b2
    global r
    r.append( (a1,b1) )

def product(lst):
  return reduce( lambda a, b: a*b, lst, 1 )

for a in range(10,100):
  for b in range(10,100):
    ad = digits(a)
    bd = digits(b)
    if a >= b: continue
    if ad[1] == 0 and bd[1] == 0:
      continue
    if ad[0] == bd[0]:
      f(a,b,ad[1],bd[1])
    if ad[0] == bd[1] and bd[0] != bd[1]:
      f(a,b,ad[1],bd[0])
    if ad[1] == bd[0] and ad[0] != ad[1]:
      f(a,b,ad[0],bd[1])
    if ad[1] == bd[1] and ad[0] != ad[1] and bd[0] != bd[1]:
      f(a,b,ad[0],bd[0])

print
print r
numerator = product( [ e[0] for e in r ] )
denominator =  product( [ e[1] for e in r ] )
g = gcd( numerator, denominator )

print denominator / g


