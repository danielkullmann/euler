#!/usr/bin/env python

"""
sudoku solver
"""

from common import *
import os, sys, cPickle

def r9(): return range(0,9)
def r3(): return range(0,3)
def n10(): return range(1,10)

""" list of all positions of all regions """
regions = []
# linear regions
for i in r9():
  regions.append( [(i,j) for j in r9() ] )
  regions.append( [(j,i) for j in r9() ] )

# block regions
for bx in r3():
  for by in r3():
    r = [ (3*bx+sx,3*by+sy) for sx in r3() for sy in r3() ]
    regions.append(r)

""" list of all positions in sudoku """
allPos = [ (i,j) for i in r9() for j in r9() ]

""" maps positions to the indexes of all regions they are part of """
posToRegions = {}
for p in allPos:
  rs = []
  for i,r in enumerate(regions):
    if p in r: rs.append(i)
  posToRegions[p] = rs

def get(p,s):
  """ get value at position p in sudoku s """
  assert( type(p) == type((1,)) )
  assert len(p) == 2
  assert len(s) == 9
  assert len(s[0]) == 9
  return s[p[0]][p[1]]

def setv(p,v,s):
  """ set position p to value v in sudoku s """
  assert( type(p) == type((1,)) )
  assert len(p) == 2
  assert( type(v) == type(1) )
  assert len(s) == 9
  assert len(s[0]) == 9
  s[p[0]][p[1]] = v

def getr(r,s):
  """ returns all values in region r in sudoku s """
  assert( type(r) == type([]) )
  assert len(s) == 9
  assert len(s[0]) == 9
  return [ get(p,s) for p in r ]

def options(p,s):
  """ get all numbers that can be on position p in sudoku s """
  assert len(p) == 2
  assert len(s) == 9
  assert len(s[0]) == 9
  v = get(p,s)
  if v != 0: return [v]

  notResult = []
  for ri in posToRegions[p]:
    vals = getr(regions[ri],s)
    notResult = notResult + vals
  return [ v for v in n10() if v not in notResult ]
    

def solved(s):
  """ checks whether the given puzzle is solved """
  for r in s:
    for i in r:
      if i == 0:
        return False
  return True

def check(s):
  for r in regions:
    vs = getr(r,s)
    if any( [ v == 0 for v in vs ] ): 
      print vs, r
      return False
    for n in n10():
      if len( [ 1 for v in vs if v == n ] ) != 1:
        print n, vs, r
        return False
  return True


def copy(s):
  """ makes a copy of the puzzle """
  assert len(s) == 9
  assert all( [ len(r) == 9 for r in s ] )
  return [ l[:] for l in s ]

def out(s):
  for l in s: print l
  print ""


def sortPositionsBySolved(s,opts):
  """ Return list of all unsolved positions, sorted by the number of options """
  ps = [ (p,o,len(o)) for p in allPos for o in [opts[p]] if get(p,s) == 0 ]
  ps.sort( lambda a,b: cmp( a[2], b[2] ) )
  ps = [ (p[0],p[1]) for p in ps ]
  return ps

def solve(s,no,lvl=1):
  """ tries to solve the puzzle """
  #print lvl,
  while not solved(s):
    changed = False
    
    opts = {}
    for p in allPos: opts[p] = options(p, s)

    # check all positions: is there only one possibility in that position?
    for p in allPos:
      # already solved?
      if get(p,s) == 0:
        # => No!
        o = opts[p]
        if len(o) == 1:
          print "1)", p, o[0]
          setv(p,o[0],s)
          opts = {}
          for p in allPos: opts[p] = options(p, s)
          changed = True

    if changed: continue

    opts = {}
    for p in allPos: opts[p] = options(p, s)

    # check for all regions if a certain number can only be at
    # one position
    for r in regions:
      os = [ (p,opts[p]) for p in r ]
      for n in n10():
        if any( [ o[1] == [n] for o in os ] ): continue
        occurrences = [ o for o in os if n in o[1] ]
        if len(occurrences) == 1:
          p = occurrences[0][0]
          print "2)", p, n
          setv(p,n,s)
          opts = {}
          for p in allPos: opts[p] = options(p, s)
          changed = True

    opts = {}
    for p in allPos: opts[p] = options(p, s)

    # try out a number on a point:
    if not changed and not solved(s):
      ps = sortPositionsBySolved(s,opts)
      isSolved = False
      s1 = None
      for (p,os) in ps:
        if get(p,s) != 0: raise Exception("No2")
        for n in os:
          s1 = copy(s)
          setv(p,n,s1)
          print "3)", p, n
          s1 = solve(s1,no,lvl+1)
          if solved(s1):
            return s1

    # no changes possible: break out of loop
    if not changed: break

  return s


if os.path.exists("sudoku.dat"):
  with open("sudoku.dat", "r") as fh: problems = cPickle.load( fh )
else:
  # Read in problems from text file
  fh = open("sudoku.txt", "r")
  lines = fh.readlines()
  fh.close()

  no = 0
  problems = {}
  problem = None
  for l in lines:
    l = l.strip()
    if l[:5] == "Grid ":
      if problem != None:
        problems[no] = ((problem,None))
      problem = []
    else:
      problem.append( [int(c) for c in l ] )
    no += 1

  problems[no] = ((problem,None))

#print len(problems)

sum = 0
cnt = 0
pcnt = 0
for no in problems.keys():
  (sudoku,sol) = problems[no]
  print "==============", pcnt
  if sol == None:
    sol = copy(sudoku)

  if not solved(sol):
    out(sol)
    sol = solve(sol,no)

  if solved(sol):
    if not check(sol):
      out(sol)
      raise Exception("No")
    num = makeNum(sol[0][:3])
    out(sol)
    print "Solved", num
    sum += num
    cnt += 1

  problems[no] = (sudoku,sol)
  with open("sudoku.dat", "w") as fh: cPickle.dump( problems, fh )
  pcnt += 1

print "=============="
print "sum", sum
print "cnt", cnt


