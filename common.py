import os, math
import fractions
import itertools

gcd = fractions.gcd
permutations = itertools.permutations
iterproduct = itertools.product

def primeGenerator( _primes = [2,3] ):
  for p in _primes: yield p
  nextNum = _primes[-1] + 2
  while True:
    idx = 0
    stop = int(math.sqrt(nextNum))+1
    l = len(_primes)
    isPrime = True
    while idx < l and _primes[idx] < stop:
      if nextNum % _primes[idx] == 0:
        isPrime = False
        break
      idx += 1
    if isPrime:
      _primes.append( nextNum )
      yield  nextNum 
    nextNum += 2


def generatePrimes(primes, num):
  nextNum = 3
  if len(primes) <= 1:
    primes = [2]
  else:
    nextNum = primes[-1] + 2

  while nextNum < num:
    idx = 0
    stop = int(math.sqrt(nextNum))+1
    l = len(primes)
    isPrime = True
    while idx < l and primes[idx] < stop:
      if nextNum % primes[idx] == 0:
        isPrime = False
        break
      idx += 1
    if isPrime:
      primes.append( nextNum )
    nextNum += 2
  return primes


def makePrimes(maxnum):
  import cPickle as pckle
  #import pickle as pckle
  result = []
  if (os.path.exists("primes.dat")): 
    afile = open("primes.dat", "r")
    result = pckle.load(afile)
    afile.close()
  if len(result) == 0 or result[-1] < maxnum:
    result = generatePrimes(result, maxnum)
  afile = open("primes.dat", "w")
  pckle.dump( result, afile )
  afile.close()
  r = []
  idx = 0
  while idx < len(result) and result[idx] <= maxnum:
    r.append(result[idx])
    idx+=1
  return r

def makePrimes2(minnum, maxnum):
  import cPickle
  result = []
  if (os.path.exists("primes.dat")): 
    afile = open("primes.dat", "r")
    result = cPickle.load(afile)
    afile.close()
  if result[-1] < maxnum:
    result = generatePrimes(result, maxnum)
  afile = open("primes.dat", "w")
  cPickle.dump( result, afile )
  afile.close()
  r = []
  idx = 0
  while idx < len(result) and result[idx] <= maxnum:
    if minnum <= result[idx]: r.append(result[idx])
    idx+=1
  return r

def primeFactors(n,primes):
  f = []
  for p in primes:
    if n%p == 0:
      c = 0
      while n%p == 0:
        n = n/p
        c += 1
      f.append((p,c))
    if n == 1:
      break
  return f

def primeFactors1(n,primes):
  f = []
  for p in primes:
    if n%p == 0:
      while n%p == 0:
        n = n/p
      f.append(p)
    if n == 1:
      break
  return f


def divisors(n):
  if n == 1: return [1]
  return [ x for x in range(1,n/2+1) if n%x == 0]


def makeMap(lst):
  m = {}
  for e in lst: 
    m[e] = True
  return m

def ndigits(n):
  c = 1
  while n >= 10:
    n = n/10
    c += 1
  return c

def digits(n):
  if type(n) == str:
    return [ int(c) for c in n ]
  r = []
  while n > 0:
    r.append( n%10 )
    n = n / 10
  return r[::-1]

def countDigits(n):
  r = [ 0 for i in range(0,10) ]
  for d in digits(n):
    r[d] += 1
  return r

def isPanDigital(n):
  d = countDigits(n)
  if d[0] != 0: return False
  return all( [ c == 1 for c in d[1:] ] )

def isNPanDigital(n):
  d = countDigits(n)
  if d[0] != 0: return False
  return all( [ c == 1 for c in d[1:ndigits(n)] ] )

def makeNum(lst):
  n = 0
  for e in lst:
    n = n*10 + e
  return n

def product(lst):
  return reduce( lambda a,b: a*b, lst, 1 )

