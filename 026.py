#!/usr/bin/env python

from decimal import *

getcontext().prec = 2000

m = 0

def lr(s):
  m = 0
  for i in range(0,len(s)/2+1):
    for l in range(1,(len(s)-i)/2+1):
      if s[i:i+l] == s[i+l:i+l+l]:
        if l > m:
          m = l
        break
  return m

for i in range(1,1000):
  d = Decimal(1) / Decimal(i)
  s = str(d)[2:] 
  l = lr(s)
  if l > m:
    m = l
    print i,l
