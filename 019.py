#!/usr/bin/env python

from common import *
import datetime as dt

d = dt.date(1901,1,1)
while d.weekday() != 6:
  d = d + dt.timedelta(1)

e = dt.date(2000,12,31)

s = 0
while d <= e:
  if d.day == 1:
    s += 1
  d = d + dt.timedelta(7)

print s

# Solved: 171

