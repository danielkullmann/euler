#!/usr/bin/python

"""
Starting in the top left corner of a 2x2 grid, there are 6 routes (without
backtracking) to the bottom right corner.

How many routes are there through a 20x20 grid?
"""

# I have to go 20 times right, and 20 times down
# This means if a 20 zeroes, and twenty ones, 
# and want to know how many different strings 
# I can make from them
# Is that 40! / 20! / 20! ?
# (40! ways to make a 40-character string, and then
#  I have to discount the number of permutations in
#  the zeroes substring and the ones substring)
# Example:
# 4! / 2! / 2! = 4*3*2*1 / ( 2*1 * 2*1 ) = 6, which is right

from math import factorial as f

print f(40) / f(20)**2

# Solved: 137846528820

