#!/usr/bin/env python

import sys
from common import *

min = 100000000000000000000
n = 1
pentagons = []
while True:
  c = n*(3*n-1)/2
  for a in pentagons:
    b = c - a
    d = max(a-b,b-a)
    if b in pentagons and d in pentagons:
      print d, a, b, c
      if d < min:
        min = d
        print "==", d, a, b, c
  pentagons.append(c)
  if len(pentagons) % 100 == 0:
    sys.stdout.write(".")
    sys.stdout.flush()
    if len(pentagons) % 8000 == 0:
      sys.stdout.write("\n")
  n += 1

