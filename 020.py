#!/usr/bin/env python

import math

print sum([ int(c) for c in str(math.factorial(100)) ])

# Solved: 648

