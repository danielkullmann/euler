#!/usr/bin/env python

from common import *

fh = open("names.txt", "r")
lines = fh.readlines()
fh.close()

names = "".join(lines).split(",")
names = [ n.replace('"', '') for n in names ]
names.sort()

s = 0
for i,n in enumerate(names):
  v = sum( [ ord(c) - ord ('A') + 1 for c in n ] )
  print i, n, v
  s += v*(i+1)

print s


