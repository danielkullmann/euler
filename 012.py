#!/usr/bin/env python

number = 0
index = 0
maxd = 0

while True:
  index += 1
  number += index

  k = 1
  numd = 0
  while k < number:
    if number % k == 0: numd += 1
    k += 1

  if numd > maxd:
    maxd = numd
    print "k", index, number, maxd

  if numd >= 500:
    print number
    break


