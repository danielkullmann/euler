#!/usr/bin/env python

from common import *
from fractions import Fraction as F

def calculator(s):
  try:
    stack = []
    for c in s:
      if c in "0123456789":
        stack.append(F(c))
      elif c == "+":
        a,b = stack.pop(), stack.pop()
        stack.append(a+b)
      elif c == "-":
        a,b = stack.pop(), stack.pop()
        stack.append(a-b)
      elif c == "_":
        a,b = stack.pop(), stack.pop()
        stack.append(b-a)
      elif c == "*":
        a,b = stack.pop(), stack.pop()
        stack.append(a*b)
      elif c == "/":
        a,b = stack.pop(), stack.pop()
        if b == 0: return None
        stack.append(a/b)
      elif c == "\\":
        a,b = stack.pop(), stack.pop()
        if a == 0: return None
        stack.append(b/a)
    t = stack.pop()
    if t.denominator == 1:
      return t.numerator
    return None
  except Exception, e:
    print e
    return None

def icalculator(s):
  try:
    stack = []
    for c in s:
      if c in "0123456789":
        stack.append(int(c))
      elif c == "+":
        a,b = stack.pop(), stack.pop()
        stack.append(a+b)
      elif c == "-":
        a,b = stack.pop(), stack.pop()
        stack.append(a-b)
      elif c == "_":
        a,b = stack.pop(), stack.pop()
        stack.append(b-a)
      elif c == "*":
        a,b = stack.pop(), stack.pop()
        stack.append(a*b)
      elif c == "/":
        a,b = stack.pop(), stack.pop()
        if b == 0: return None
        stack.append(a/b)
      elif c == "\\":
        a,b = stack.pop(), stack.pop()
        if a == 0: return None
        stack.append(b/a)
    t = stack.pop()
    return t
  except Exception, e:
    print e
    return None

calcs = [''.join(x) for x in iterproduct("+-*/_\\", repeat=3)]

mx = 0

for a in range(1,7):
  for b in range(a+1,8):
    for c in range(b+1,9):
      for d in range(c+1,10):
        numbers = permutations( "%d%d%d%d" % (a,b,c,d) )
        results = {}
        for ns in numbers:
          ns = ''.join(ns)
          for cs in calcs:
            n = calculator(ns+cs)
            if n != None: results[ n ] = True
        m = 0
        while True:
          m += 1
          if m not in results: 
            m -= 1
            break
        #print "%d%d%d%d" % (a,b,c,d),m
        if m > mx:
          mx = m
          print "==%d%d%d%d" % (a,b,c,d),m,max(results.keys())


