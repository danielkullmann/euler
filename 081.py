#!/usr/bin/env python

""" 
In the 5 by 5 matrix below, the minimal path sum from the top left to the
bottom right, by only moving to the right and down, is indicated in bold red
and is equal to 2427.
	
131	673	234	103	18
201	96	342	965	150
630	803	746	422	111
537	699	497	121	956
805	732	524	37	331
	
Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target
As...'), a 31K text file containing a 80 by 80 matrix, from the top left to the
bottom right by only moving right and down.
"""

import sys
from common import *

matrix = [
  [131,	673, 234, 103,  18],
  [201,	 96, 342, 965, 150],
  [630,	803, 746, 422, 111],
  [537,	699, 497, 121, 956],
  [805,	732, 524,  37, 331],
]

def diagItems(diag, matrix):
  r = []
  size = len(matrix)
  for i in range(0,size):
    j = diag-i
    if j >= 0 and j < size:
      r.append(matrix[i][j])
  return r

# check whether assetr works as expected
try:
  assert False
  raise Exception("assert does not work")
except AssertionError:
  pass


def combine1(longer, shorter):
  """ combines two lists, returns a long one"""
  result = []
  l = len(longer)-1
  result.append(longer[0]+shorter[0])
  for i in range(1,l):
    result.append(longer[i] + min(shorter[i], shorter[i-1]))
  result.append(longer[l] + shorter[l-1])
  return result

def combine2(shorter, longer):
  """ combines two lists, returns a  short one"""
  result = []
  l = len(longer)-1
  for i in range(0,l):
    result.append(shorter[i] + min(longer[i], longer[i+1]))
  return result

def solve(matrix):
  size = len(matrix)
  numDiags = 2*size-1
  assert [matrix[0][0]] == diagItems(0, matrix)
  assert [matrix[size-1][size-1]] == diagItems(numDiags-1, matrix)
  items = diagItems(0,matrix)
  print 0, items
  for diag in range(1,numDiags):
    nitems = diagItems(diag,matrix)
    if len(items)+1 == len(nitems):
      items = combine1(nitems,items)
    elif len(items) == len(nitems)+1:
      items = combine2(nitems,items)
    else:
      raise Exception("no")
    print diag, items
  
  print items[0]

solve( matrix )

fh = open("matrix.txt", "r")
lines = fh.readlines()
fh.close()

matrix = [ [ int(c) for c in l.split(",") ] for l in lines ]

solve( matrix )

# Solved: 427337

