#!/usr/bin/env python

from math import factorial as f

def digits(n): return [int(c) for c in str(n)]
def a(n): return sum( [f(b) for b in digits(n)] )

i=3
s=0
while True:
  if i == a(i):
    s += i
    print s, i, [f(b) for b in digits(i)]
  i += 1
  # I know the largest number is 40585
  # (because I ran the script without the following lines)
  if i > 100000:
    break

