#!/usr/bin/env python

from common import *

"""
"""

from itertools import permutations

sm = 0
ps = {}

for pm in permutations([1,2,3,4,5,6,7,8,9]):
  for i in range(1,8):
    a = makeNum(pm[0:i])
    for j in range(i+1,9):
      b = makeNum(pm[i:j])
      p = makeNum(pm[j:])
      if a*b == p:
        if p not in ps:
          sm += p
        ps[p] = 1
        print a, b, p, sm

print sm

