#!/usr/bin/env python

from common import *

pr = makePrimes2(1000,9999)

for p in pr:
  for i in range(1,9999-p):
    lst = [ p+j*i for j in [0,1,2] ]
    if all( [ e in pr for e in lst ]):
      ds = [ countDigits(e) for e in lst ]
      if all( [ e == ds[0] for e in ds ] ):
        print i, lst

