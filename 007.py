#!/usr/bin/python

"""
"""

primes = [2]
n=3
while True:
  prime = True
  for p in primes:
    if (n % p) == 0:
      prime = False
      break
  if prime:
    primes.append(n)
    if len(primes) == 10001:
      print n
      break
  # 2 is the only even prime
  n += 2

# Solved: 104743

