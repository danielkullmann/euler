#!/usr/bin/env python

from common import *

def amicable(a):
  b = sum(divisors(a))
  c = sum(divisors(b))
  return a == c and a != b

s = 0
for i in range(1,10000):
  if amicable(i):
    print i
    s += i

print s

# Solved: 31626


