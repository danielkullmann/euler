#!/usr/bin/env python

from common import *
import sys

sm = 0

yays = {}
nays = {}

for a in range(1,10*1000*1000):
  b = a
  chain = []
  yay = False
  while b != 89:
    if b in chain:
      chain.append(b)
      for c in chain: 
        if c < 500: nays[c] = True
      break
    chain.append(b)
    if b in yays:
      chain.append("yay")
      yay = True
      break
    if b in nays:
      chain.append("nay")
      for c in chain: 
        if c < 500: nays[c] = True
      break
    b = sum( [ d*d for d in digits(b) ] )
  if yay or b == 89:
    if not yay:
      chain.append(b)
    for c in chain: 
      if c < 500: yays[c] = True
    sm += 1

print sm


