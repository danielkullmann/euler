#!/usr/bin/env python

from common import *

mx = 2

pr = makePrimes(100000)

def combineMap(m1,m2):
  m = {}
  for k in m1.keys():
    m[k] = m1[k]
  for k in m2.keys():
    m[k] = m2[k]
  return m

n = 2
while True:
  j = 0
  pfs = []
  while True:
    pf = primeFactors(n+j,pr)
    if len(pf) != mx:
      break
    pfs.append(pf)
    if j+1 == mx:
      print [n+i for i in range(0,j+1)], pfs
      mx += 1
      break
    j += 1
  n += 1
  if mx == 5: break

