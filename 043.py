#!/usr/bin/env python

from common import *

numbers = permutations(digits(1234567890))

divisors= [0,2,3,5,7,11,13,17]

def check(d):
  for i in range(1,8):
    sub = 100*d[i] + 10*d[i+1] + 1*d[i+2]
    if sub % divisors[i] != 0: return False
  return True

sm = 0
for n in numbers:
  m = makeNum(n)
  if check(n):
    print m
    sm += m

print sm

