#!/usr/bin/env python

import sys

# Sum of a and b: 2-999
for ab in range( 2, 1000 ):

  for a in range(1,ab):
    b = ab-a
    c = 1000-ab

    if a*a + b*b == c*c:
      print a, b, c, a*b*c
      sys.exit()

# Solved: 200 375 425 31875000

