import java.math.BigInteger;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.math.MathContext;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class P243 {

  private static final int THREADS = 3;
  private static final BigInteger one = new BigInteger( "1" );
  private static final BigInteger two = new BigInteger( "2" );
  private static final Pair target = new Pair( new BigInteger( "15499" ), new BigInteger( "94744" ) );
  private static final MathContext mc = new MathContext( 4, RoundingMode.HALF_EVEN );
  private static volatile Pair best = new Pair( one, one );
  private static volatile boolean finished;
  private static volatile int tasks = 0;
  private static Object syncer = new Object();
  

  
  private static Pair resilience( BigInteger n ) {
    BigInteger cnt = new BigInteger( "0" );
    for ( BigInteger i = new BigInteger( "1" ); i.compareTo( n ) < 0; i = i.add( one ) ) {
      if ( n.gcd( i ).compareTo( one ) == 0 ) {
        cnt = cnt.add( one );
      }
    }
    return new Pair( cnt, n.subtract( one ) );
  }

  /**
   * @param args
   */
  public static void main( String[] args ) {
    BigInteger start = one;
    if (args.length > 0) {
      start = new BigInteger( args[0] );
    }
    BigDecimal ratio = new BigDecimal( target.v1);
    ratio = ratio.divide( new BigDecimal( target.v2 ), mc );
    System.out.println( "target: " + ratio.doubleValue() );
    ExecutorService pool = Executors.newFixedThreadPool( THREADS );
    BigInteger p = new BigInteger( "2" );
    BigInteger d = new BigInteger( "3" );
    finished = false;
    while ( ! finished ) {
      BigInteger p2 = p.multiply( d ).divide( p.gcd( d ) );
      if ( p2.compareTo( p ) != 0 ) {
        p = p2;
        if ( d.compareTo(start) > 0 ) {
          waitForTasks();
          tasks++;
          pool.execute( new Runner(d, p) );
        }
      }
      d = d.add( one );
    }
  }

  private static void waitForTasks() {
    while ( tasks > 1 ) {
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        throw new IllegalArgumentException( e );
      }
    }
  }

  private static class Runner implements Runnable {

    private BigInteger d;
    private BigInteger p;

    public Runner( BigInteger d, BigInteger p ) {
      this.d = d;
      this.p = p;
    }

    @Override
    public void run() {
      tasks--;
      Pair res = resilience( p );
      //if (res.v1.multiply(best.v2).compareTo(best.v1.multiply(res.v2))<0) {
      //  best = res;
        BigDecimal ratio = new BigDecimal( res.v1);
        ratio = ratio.divide( new BigDecimal( res.v2 ), mc );
        synchronized( syncer ) {
          System.out.println( "== " + d + " : " + p + " : " + res.v1 + "/" + res.v2 + " => " + ratio.doubleValue() );
        }
      //}
      if ( res.v1.multiply( target.v2 ).compareTo( target.v1.multiply( res.v2 ) ) < 0 ) {
        synchronized( syncer ) {
          System.out.println( ">> " + d + " : " + p + " : " + res.v1 + "/" + res.v2 + " => " + ratio.doubleValue() );
        }
        finished = true;
      }
    }
    
  }

  private static class Pair {
    public BigInteger v1;
    public BigInteger v2;

    public Pair( BigInteger v1, BigInteger v2 ) {
      super();
      this.v1 = v1;
      this.v2 = v2;
    }

  }
}
