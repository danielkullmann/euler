#!/usr/bin/env python

tri = [
  [75],
  [95,64],
  [17,47,82],
  [18,35,87,10],
  [20,04,82,47,65],
  [19,01,23,75,03,34],
  [88,02,77,73,07,63,67],
  [99,65,04,28,06,16,70,92],
  [41,41,26,56,83,40,80,70,33],
  [41,48,72,33,47,32,37,16,94,29],
  [53,71,44,65,25,43,91,52,97,51,14],
  [70,11,33,28,77,73,17,78,39,68,17,57],
  [91,71,52,38,17,14,91,43,58,50,27,29,48],
  [63,66,04,68,89,53,67,30,73,16,69,87,40,31],
  [04,62,98,27,23, 9,70,98,73,93,38,53,60,04,23]]

routes = 0

def findmax(x,y):
  v = tri[y][x]
  if y == len(tri)-1:
    global routes
    routes += 1
    return v
  s1 = findmax(x,y+1)
  s2 = findmax(x+1,y+1)
  return v + max(s1, s2)

def diffs():
  diffs = []
  diffsum = 0
  for y in range(0,len(tri)):
    diff = max(tri[y]) - min(tri[y])
    diffsum += diff
    diffs.append( diffsum )
  return diffs


def findmax2():
  # copy of last tri row
  values = tri[len(tri)-1][:]
  for y in range(len(tri)-2,-1,-1):
    row = tri[y]
    new = []
    for x in range(0,len(row)):
      new.append( row[x] + max(values[x], values[x+1]) )
    values = new
    print values
    pass
  return values[0]

print findmax(0,0)
print routes

print findmax2()

# Solved: 1074


