#!/usr/bin/env python

from common import *

s = ""
i = 1

while len(s) < 1000000:
  s = s + str(i)
  if i == 20: print s
  i += 1

print product( [ int(s[c]) for c in [0, 9, 99, 999, 9999, 99999, 999999] ] )


