#!/usr/bin/env python

from common import *

def find(p):
  sols = []
  numSols = 0
  for a in range(1,p-2):
    for b in range(a,p-a-1):
      c = p-a-b
      if a*a+b*b == c*c:
        sols.append((a,b,c))
        numSols += 1
  return (sols,numSols)

mx = 0
for p in range(1,1001):
  sl, ns = find(p)
  if ns > mx:
    mx = ns
    print p, ns, sl
 
