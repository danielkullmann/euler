#!/usr/bin/python

"""
"""

import sys


def digits(n): return [int(c) for c in str(n)]

primes = [2]
n=3
circulars = [2] # 2 is a circular prime
while n < 1000000:
  prime = True
  for p in primes:
    if (n % p) == 0:
      prime = False
      break
  if prime:
    primes.append(n)
    if len(primes) % 100 == 0: 
      sys.stdout.write(".")
      if len(primes) % 8000 == 0:
        sys.stdout.write("\n")
      sys.stdout.flush()
  # 2 is the only even prime
  n += 2

print 
print len(primes), "primes found"

for n in primes:
  d = digits(n)
  # skip primes that are larger than 10 and that have an even digit or 5 as a digit
  # because one of the variations will be an even number or divisible by 5.
  if n < 10 or all( [ a in [1,3,7,9] for a in d ] ):
    isCircular = True
    for j in range(1,len(d)):
      ts = d[j:] + d[:j]
      t = int(''.join( [str(c) for c in ts] ))
      if t not in primes:
        #print "  # not", t, "for", n
        isCircular = False
        break
    if isCircular:
      circulars.append(n)
      print len(circulars), n

print len(circulars)
circulars.sort()
print circulars
