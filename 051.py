#!/usr/bin/env python

"""
By replacing the 1st digit of *3, it turns out that six of the nine possible
values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit
number is the first example having seven primes among the ten generated
numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and
56993. Consequently 56003, being the first member of this family, is the
smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily
adjacent digits) with the same digit, is part of an eight prime value family.
"""

import sys
from common import *

pr = makePrimes(1000000)
#pr = primeGenerator(pr)

def numDiffs(n1,n2):
  """ for two numbers, returns the indices for which the two numbers differ"""
  d1 = digits(n1)
  d2 = digits(n2)
  if len(d1) != len(d2): return []
  r1 = None
  r2 = None
  idxs = []
  for idx in range(0,len(d1)):
    if d1[idx] != d2[idx]:
      if r1 == None:
        r1 = d1[idx]
        r2 = d2[idx]
        idxs.append(idx)
      elif r1 != d1[idx] or r2 != d2[idx]:
        return []
      else:
        idxs.append(idx)
  return idxs

k = 10000

def change( digits, value, indices ):
  for i in indices:
    digits[i] = value
  return makeNum(digits)

while True:
  primes = makePrimes2(k, 10*k)
  primeMap = makeMap(primes)
  for p in primes:
    dc = countDigits(p)
    mult = [ i for i in range(len(dc)) if dc[i] >= 2 ]
    for d in mult:
      ds = digits(p)
      idxs = [ i for i in range(len(ds)) if ds[i] == d ] 
      cnt = 0
      family = []
      for v in range(0,10):
        chgd = change(ds, v, idxs )
        if chgd in primeMap:
          cnt += 1
          family.append(chgd)
      if cnt >= 8:
        print p, d, idxs, family
        import sys
        sys.exit(1)
  k*=10

# Solved: 121313 1 [0, 2, 4] [121313, 222323, 323333, 424343, 525353, 626363, 828383, 929393]

