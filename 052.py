#!/usr/bin/env python

"""
It can be seen that the number, 125874, and its double, 251748, contain exactly
the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x,
contain the same digits.
"""

from common import *

n = 1
while True:
  ds = [countDigits(k*n) for k in range(1,7) ]
  if all( [ e == ds[0] for e in ds ] ):
    print [ k*n for k in range(1,7) ]
    break
  n += 1

# Solved: [142857, 285714, 428571, 571428, 714285, 857142]

