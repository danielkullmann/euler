#!/usr/bin/env python

"""
"""

from common import *

primes = makePrimes(1000000)

sm = 0

for p in primes:
  if p < 10: continue

  truncable = True

  pn = p
  while pn > 0:
    if pn not in primes:
      truncable = False
      break
    pn = pn/10

  if not truncable: continue
  pn = p
  while pn > 0:
    if pn not in primes:
      truncable = False
      break
    pn = makeNum( digits(pn)[1:] )

  if not truncable: continue

  sm += p
  print p

print sm

