#!/usr/bin/env python

from common import *

pr = makePrimes(100000)
dsquares = [ 2*i*i for i in range(1,100) ]

n = 3
while True:
  if n not in pr:
    sumFound = False
    for p in pr:
      if n-p in dsquares:
        #print n, p, n-p
        sumFound = True
        break
    if not sumFound:
      print n
      break
  n += 2
