#!/usr/bin/env python

"""
"""

from common import *

for a in range(9,10):
  s = ""
  for n in range(1,5):
    s += str(a*n)
    if len(s) == 9:
      if isPanDigital(s):
        print s, a, n
      break

for a in range(90,100):
  s = ""
  for n in range(1,5):
    s += str(a*n)
    if len(s) == 9:
      if isPanDigital(s):
        print s, a, n
      break

for a in range(900,1000):
  s = ""
  for n in range(1,5):
    s += str(a*n)
    if len(s) == 9:
      if isPanDigital(s):
        print s, a, n
      break

for a in range(9000,10000):
  s = ""
  for n in range(1,5):
    s += str(a*n)
    if len(s) == 9:
      if isPanDigital(s):
        print s, a, n
      break
