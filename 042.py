#!/usr/bin/env python

from common import *

fh = open("words.txt", "r")
lines = fh.readlines()
fh.close()

words = "".join(lines).split(",")
words = [ n.replace('"', '') for n in words ]
words.sort()

ml = max( [ len(w) for w in words ] )
maxValue = ml*26

print ml, maxValue

ts = []
n = 1
while True:
  t = n*(n+1)/2
  ts.append(t)
  if t > maxValue: break
  n = n+1

print ts

cnt = 0
for i,n in enumerate(words):
  v = sum( [ ord(c) - ord ('A') + 1 for c in n ] )
  if v in ts: cnt += 1

print cnt


