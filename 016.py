#!/usr/bin/env python

import math

# number of digits in 2^1000
# = upper(log10(2^1000)) = upper(1000*log10(2))

#print 1000*math.log(2)

n = str(2**1000)
s = 0
for c in n:
  s += int(c)

print s

# Solved: 1366

